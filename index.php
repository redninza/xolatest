<?php

/**
 * Taking input from command line
 */

/** GRID SIZE */
echo "\n\033[33mPlease insert the room grid (separated by space e.g.: 5 5 ): ";
$handle = fopen ("php://stdin","r");
$line = fgets($handle);

$gridArray = explode(' ', trim($line));

if ((count($gridArray) < 1) || (count($gridArray) > 2)) {
    echo "\n\033[31mInvalid grid size!!\n";
    exit;
}


/** NUMBER OF ANTS DEPLOYED */
echo "\n\033[33mPlease insert number of spy ants deployed: ";
$handle = fopen ("php://stdin","r");
fscanf(STDIN, "%d\n", $numberOfAnts);

$directions = $currentPosition = array();

for ($i = 0; $i < $numberOfAnts; $i++) {
    echo "\n\033[33mPlease insert current position of the spy ant#".($i+1)."
    (separated by spaces - e.g.: 1 2 N ): ";
    $handle = fopen ("php://stdin","r");
    $line1 = fgets($handle);

    $currentPosition[$i] = explode(' ', trim($line1));

    if (count($currentPosition[$i]) != 3) {
        echo "\n\033[31mInvalid location!!\n";
        exit;
    }

    echo "\n\033[33mPlease insert set of direction for the spy ant#".($i+1)."
    (without any space - e.g.: LLFRLF ): ";
    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);

    $directions[$i] = str_split($line);
}



/**
 * Processing the input
 */

require_once 'calculate.php';

$calculator = new \Xola\Calculate($gridArray);

for ($j = 0; $j < $numberOfAnts; $j++) {
    $newPosition = $currentPosition[$j];

    try{
        foreach ($directions[$j] as $direction) {
            if ($direction) {
                $newPosition = $calculator->faceOrMove($newPosition, $direction);
            }
        }

    } catch (\OutOfBoundsException $e) {
        echo "\n\033[31m".$e->getMessage()."\n";
    }

    echo "\nFinal Position of spy ant#".($j+1)." is: ". implode(' ', $newPosition) . "\n";
}