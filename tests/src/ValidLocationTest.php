<?php

namespace Xola;

class ValidLocationTest extends BaseTest
{

    public function setUp()
    {
        $this->init();
        $this->currentPosition = $this->positionMatrix[0];
        $this->sequence = 'LFLFLFLFF';
    }

    public function testAntMovesToValidPositionSuccessfully()
    {
        foreach (str_split($this->sequence) as $direction) {
            $this->currentPosition = $this->calculator->faceOrMove($this->currentPosition, $direction);
        }

        $this->assertEquals($this->currentPosition[0], 1);
        $this->assertEquals($this->currentPosition[1], 3);
        $this->assertEquals($this->currentPosition[2], 'N');
    }
}
