<?php

namespace Xola;

class ImpossibleLocationTest extends BaseTest
{

    public function setUp()
    {
        $this->init();
        $this->currentPosition = $this->positionMatrix[1];
        $this->sequence = 'LLLLLF';
    }

    /**
     * @expectedException \OutOfBoundsException
     *
     */
    public function testAntMovesToImpossibleLocationThrowsException()
    {
        foreach (str_split($this->sequence) as $direction) {
            $this->currentPosition = $this->calculator->faceOrMove($this->currentPosition, $direction);
        }
    }
}
