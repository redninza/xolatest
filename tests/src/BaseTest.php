<?php

namespace Xola;

require_once __DIR__."/../../calculate.php";

abstract class BaseTest extends \PHPUnit_Framework_TestCase
{
    protected $positionMatrix = array(
        array(1, 2, 'N'),
        array(0, 0, 'N'),
        array(3, 3, 'E')
    );

    protected $grid = array(5, 5);

    protected $sequence;

    protected $currentPosition;

    protected $calculator;

    public function init()
    {
        $this->calculator = new Calculate($this->grid);
    }
}