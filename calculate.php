<?php

namespace Xola;

class Calculate
{
    public $grid = array();

    public function __construct($grid)
    {
        $this->grid = $grid;
    }

    public function faceOrMove($position, $direction)
    {
        $direction = strtoupper($direction);

        if (empty($position)) {
            return array(0, 0, 'N');
        }

        $newPosition = $position;
        switch ($direction) {
            case 'L':
                switch(strtoupper($position[2])) {
                    case 'N' : $newPosition[2] = 'W'; break;
                    case 'S' : $newPosition[2] = 'E'; break;
                    case 'E' : $newPosition[2] = 'N'; break;
                    case 'W' : $newPosition[2] = 'S'; break;
                    default  : break;
                }
                break;

            case 'R':
                switch(strtoupper($position[2])) {
                    case 'N' : $newPosition[2] = 'E'; break;
                    case 'S' : $newPosition[2] = 'W'; break;
                    case 'E' : $newPosition[2] = 'S'; break;
                    case 'W' : $newPosition[2] = 'N'; break;
                    default  : break;
                }
                break;

            case 'F':
                switch(strtoupper($position[2])) {
                    case 'N' : $newPosition[1]++; break;
                    case 'S' : $newPosition[1]--; break;
                    case 'E' : $newPosition[0]++; break;
                    case 'W' : $newPosition[0]--; break;
                    default  : break;
                }
                break;
            default : break;
        }

        if (!$this->checkIfInvalidPosition($newPosition)) {
            throw new \OutOfBoundsException('Impossible Location!!');
        }
        return $newPosition;
    }

    public function checkIfInvalidPosition($position)
    {
        if (   ($position[0] < 0) || ($position[1] < 0)
            || ($position[0] > $this->grid[0]) || ($position[1] > $this->grid[0])) {
            return false;
        }

        return true;
    }
}